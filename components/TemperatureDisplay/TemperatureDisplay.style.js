import { StyleSheet } from "react-native";

const s = StyleSheet.create({
    text: {
        color: "white",
        fontSize: 80,
    },
});

export { s };